//
//  ViewController.swift
//  Panel de Control
//
//  Created by Master Móviles on 10/10/16.
//  Copyright © 2016 Master Móviles. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var hipervelocidadLabel: UILabel!
    @IBOutlet weak var hipervelocidadSlider: UISlider!
    @IBOutlet weak var emergenciaButton: UIButton!
    @IBOutlet weak var emergenciaSwitch: UISwitch!
    @IBOutlet weak var emergenciaLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        textView.text = ""
        hipervelocidadLabel.text = String(hipervelocidadSlider.value)
        emergenciaSwitch.isOn = true
        
        emergenciaLabel.text = "Control de emergencia \n\(isActivo(emergenciaSwitch.isOn))"
    }

    func isActivo(_ element: Bool) -> String {
        if element {
            return "Activo"
        } else {
            return "Inactivo"
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func introPulsado(_ sender: AnyObject) {
        textView.text?.append(sender.text + "\n")
        textField.text = ""
    }
    @IBAction func onSliderChange(_ sender: AnyObject) {
        hipervelocidadLabel.text = String(hipervelocidadSlider.value)
    }

    @IBAction func onEmergencyClick(_ sender: AnyObject) {
        let alert = UIAlertController(title: "Emergencia",
                                  message: "¿Qué hacer?",
                                  preferredStyle: .actionSheet)
        let nave = UIAlertAction(title: "Nave salvavidas", style: .default) {
            action in
            print("nave Salvavidas")
        }
        let hiperespacio = UIAlertAction(title: "Hiperespacio", style: .default) {
            action in
            print("Hiperespacio")
        }
        let autodestruccion = UIAlertAction(title: "Autodestrucción", style: .destructive) {
            action in
            print("Autodestrucción")
        }
        let cancel = UIAlertAction(title: "Cancelar", style: .cancel) {
            action in
            print("Cancelar")
        }
        alert.addAction(nave)
        alert.addAction(hiperespacio)
        alert.addAction(autodestruccion)
        alert.addAction(cancel)
        
        self.present(alert, animated: true)
    }

    @IBAction func onSwitchChange(_ sender: AnyObject) {
        emergenciaButton.isEnabled = emergenciaSwitch.isOn
        
        emergenciaLabel.text = "Control de emergencia \n\(isActivo(emergenciaSwitch.isOn))"
    }
}

